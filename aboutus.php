<?php
include 'header.php';
include 'connection/dbconfig.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mega Jobs - Job Details</title>

    <!-- Bootstrap -->
    <link href="css/vendors/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Font Awesome for icon fonts -->
    <link href="css/vendors/font-awesome.min.css" rel="stylesheet">
    <!-- Google Font API for Lato and Montserrat font families -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Montserrat:400,700" rel="stylesheet">
    <!-- CSS for slick slider plugin -->
    <link href="css/vendors/slick.css" rel="stylesheet">
    <link href="css/vendors/slick-theme.css" rel="stylesheet">
    <!-- Main Custom CSS file -->
    <link href="css/app.css" rel="stylesheet" type="text/css" />
  </head>

      <!-- BANNER ENDS -->
      <section class="inner-banner padding-bottom-10">
      <!-- BANNER STARTS -->
      <div class="container">
		<h4 style="color: #ffffff">JOIN US & EXPLORE THOUSANDS OF JOBS</h4>
        <div class="row">
          <div class="col-md-12">
            <!-- JOB SEARCH FORM STARTS -->
                        <form action="searchjobs.php" method="POST" class="form-inline">
              <div class="form-group keyword">
                <input type="text" class="form-control" name="jobtitle" placeholder="Enter job title">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control" name="experience" placeholder="Exp (years)">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control"name="location" placeholder="Locations">
              </div>
              <div class="input-group">
                <i class="fa fa-search"></i>
                <input type="submit" name="submit" value="Find">
              </div>
            </form>
            <!-- JOB SEARCH FORM ENDS -->
          </div>
        </div>
		</div>
		</section>
    <section class="aboutus">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <h3 style="    text-align: center;font-size: 3em;color: deepskyblue;font-weight: 600; margin: 0px 0px 2em 0px;">ABOUT US</h3>
	      	   	      <p>Established in 2014, with the head office in Trivandrum,Jobcaps operate in a variety of industries and have a clientlist that ranges from start-ups/SMEs to multinational firms. Our team of HR executives  and supporting staff offer expertise and excellence in execution, they are committed to giving customers 100% focus and satisfaction.You’ll find jobs and candidates at jobcaps. We love supporting new and experienced managers to get the best out of their people. Also we work closely with Candidates who are seeking the right opportunity and career. We continuously keep adding value to our line of services with an increasing productivity at every stage of employment life cycle. We offer highly professional and refreshingly personal HR consultancy and training for our clients.

</p>

	      	   	      <P>We provide our expertise from attracting and selecting one-of-a- kind candidate, to integrating and managing entire recruitment process, improving employee performance, developing future leaders, and managing redeployment and outplacement programs. Job Caps HR consultancy service is flexible. You can hire us on an ad hoc, project interim or retained basis. We can be a genuine part of your team or a reliable source of regular expertise. You will experience quality in everything we do for you, wherever you need us. We work with you to be that regular and consistent force. 

</P>
            
          </div>
          <div style="top:170px;"class="col-md-6 col-md-offset-1">
            <img src="img/about us.png" alt="About Us" class="bordered">
          </div>
        </div>
      </div>
    </section>
    <section class="browse-jobs">
      <div class="container">
        <h2 class="text-center">Our Service Divisions</h2>
        <div class="row">
          <div class="col-sm-4">
            <div class="team-member">
              <img src="img/trade1.jpg" alt="Team Member">
              <div class="team-mate-intro">
                <h4>Jobcaps trade and labours</h4>

                <p>Jobcaps HR Consultancy are specialist hiring to the
construction industry across the country. Our industrial
division provides skilled temporary, contract and
permanent staff.
We have the capability and experience in sourcing
construction professionals to director level . We source
professionals at all levels and job types including
commercial and residential construction, civil
construction and resources construction, local
government and project management consultancy.
</p>

              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img src="img/profesionals.jpg" alt="Team Member">
              <div class="team-mate-intro">
                <h4>Jobcaps Professionals</h4>

                <p>Jobcaps has targeted marketing and interviewing techniques to achieve incomparable results and outcomes that our clients and candidates demand. We pride ourselves on pro-actively matching niche professionals with hard-to-fill positions. We have built up an enviable network of professionals for all your staffing and hiring needs. We promise to deliver clients with exemplary records and qualifications to offer you a tailor made solution to your employment needs.</p>

              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img src="img/hospitalities.jpg" alt="Team Member">
              <div class="team-mate-intro">
                <h4>Jobcaps   Hospitality</h4>

                <p>Jobcaps Hospitality is our specialist division serving the needs of the hotel, catering, hospitality and event management industries.  We supply a premier service across the country. This industry has varying degrees of competition and sophistication so now more than ever requires professional, experienced, commercially astute top talent to grow market share. As this market is highly competitive, and in some sectors growing at a phenomenal pace, it is critical to source and select the best people.</p>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- FOOTER STARTS -->
       <?php
    include 'footer.php';
    ?>