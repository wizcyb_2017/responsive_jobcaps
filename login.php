<?php
include 'header.php';
include 'connection/dbconfig.php';
?>
    <section class="signup-signin">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <h1 class="text-center"><span style="float:left;">SEEKER LOGIN </span> <a style="color:orange; font-size:18px; float:right;" href="employerlogin.php">  EMPLOYER LOGIN</a></h1>
            <div class="signin-form">
              <form>
                <div class="form-group">
                  <input type="text" name="username" class="form-control" placeholder="User Name" required />
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control" placeholder="password" required />
                </div>
                <div class="form-group checkbox">
                  <input type="checkbox" id="logged-in" />
                  <label for="logged-in">Keep me signed in</label>
                  <a href="javascript:void(0);">Forgot Password?</a>
                </div>
                <input type="submit" name="submit" class="btn btn-primary btn-secondary btn-block" value="Sign In" />
              </form>
              <p class="register-link text-center">Not a member yet? <a href="seekerregistration.php">Register Now</a></p>
            </div>
            <p class="desclimer-text text-center">In case you are using a public/shared computer we recommend that<br>
            you logout to prevent any un-authorized access to your account</p>
          </div>
        </div>
      </div>
    </section>
    <?php
    include 'footer.php';
    ?>