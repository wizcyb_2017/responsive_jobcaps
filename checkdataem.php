<?php
include 'header.php';
include 'connection/dbconfig.php';


 ?>

    <section class="inner-banner">
      <!-- BANNER STARTS -->
      <div class="container">
        <h4 style="color: #ffffff">JOIN US & EXPLORE THOUSANDS OF JOBS</h4>
        <div class="row">
          <div class="col-md-12">
            <!-- JOB SEARCH FORM STARTS -->
            <form action="searchjobs.php" method="POST" class="form-inline">
              <div class="form-group keyword">
                <input type="text" class="form-control" name="jobtitle" placeholder="Enter job title">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control" name="experience" placeholder="Exp (years)">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control"name="location" placeholder="Locations">
              </div>
              <div class="input-group">
                <i class="fa fa-search"></i>
                <input type="submit" name="submit" value="Find">
              </div>
            </form>
            <!-- JOB SEARCH FORM ENDS -->
          </div>
        </div>
      </div>
      <!-- BANNER ENDS -->
    </section>
    <section class="search-result">
      <div class="container">
        <div class="row">
          <div class="sidebar col-md-12 col-sm-8">
           <!-- <h5>Refine Search <a href="javascript:void(0);" class="pull-right">Clear All Filter</a></h5>-->
			
			
            <div style="background-color: rgb(52, 73, 94); height:auto; display:block; margin-top:10px; margin-bottom:10px; padding:10px; overflow: hidden;">

            <div style="background-color: rgb(52, 73, 94); height:auto; display:block; margin-top:10px; margin-bottom:10px; padding:10px; overflow: hidden;">

                <div style="background-color: rgb(255, 255, 255); padding:10px; overflow: hidden;">

                    <h2 style="color: rgb(159, 96, 0); font-size: 30px;" align="center"><b>Congratulations  <?php  $companyname=$_GET['id'];  echo $companyname ?>   , You Have Successfully Registered!</b>

                    </h2>

                    <h6 style="font-size: 18px;" align="center"><b>(We have sent you a verification link to your Email ID.

                     

                      Also check your Spam/Junk box to verify your Email).</b>

                    </h6>

                    <h3 align="center" style="margin-top:30px;">

                        <span class="go">Go to Your Email Account</span>

                    </h3>

                    <br>

                </div>

            </div>

            </div>
			
			
		
				
			


          </div>
		  
		  
		   
 
            <!-- <ul class="pagination pull-right">
              <li><a href="javascript:void(0);">1</a></li>
              <li><a href="javascript:void(0);">2</a></li>
            </ul> -->
          </div>
        </div>
      </div>
    </section>
    <?php
    include 'footer.php';
    ?>