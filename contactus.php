<?php
include 'header.php';
include 'connection/dbconfig.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mega Jobs - Job Details</title>

    <!-- Bootstrap -->
    <link href="css/vendors/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Font Awesome for icon fonts -->
    <link href="css/vendors/font-awesome.min.css" rel="stylesheet">
    <!-- Google Font API for Lato and Montserrat font families -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Montserrat:400,700" rel="stylesheet">
    <!-- CSS for slick slider plugin -->
    <link href="css/vendors/slick.css" rel="stylesheet">
    <link href="css/vendors/slick-theme.css" rel="stylesheet">
    <!-- Main Custom CSS file -->
    <link href="css/app.css" rel="stylesheet" type="text/css" />
  </head>

      <!-- BANNER ENDS -->
      <section class="inner-banner padding-bottom-10">
      <!-- BANNER STARTS -->
      <div class="container">
		<h4 style="color: #ffffff">JOIN US & EXPLORE THOUSANDS OF JOBS</h4>
        <div class="row">
          <div class="col-md-12">
            <!-- JOB SEARCH FORM STARTS -->
                        <form action="searchjobs.php" method="POST" class="form-inline">
              <div class="form-group keyword">
                <input type="text" class="form-control" name="jobtitle" placeholder="Enter job title">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control" name="experience" placeholder="Exp (years)">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control"name="location" placeholder="Locations">
              </div>
              <div class="input-group">
                <i class="fa fa-search"></i>
                <input type="submit" name="submit" value="Find">
              </div>
            </form>
            <!-- JOB SEARCH FORM ENDS -->
          </div>
        </div>
		</div>
		</section>

    <section class="aboutus">
	<h2 style="    color: #55d3e1;"> <center><b>CONTACT US </b>  </center></h2>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <h6>Our Mailing Address</h6>
            <address><strong>JobCaps HR Consultancy</strong>
            Shalom6, Paris Road,Bakery Junction,<br>
             Trivandrum, Kerala, Pin 695014,<br> India</address>
            <address><strong>E-Mail</strong>
            <a href="mailto:example@mail.com">contact@jobcapsindia.com </a><br>
            </address>
			            <address><strong>Call us</strong>
            Tel:0471-4099914/15/16<br>
           Tel:0471-4010042,<br>Mob:08547421221</address>
          </div>
          <div class="col-md-8">
            <h6>We Are Happy to Help You</h6>
            <form method="post" action="contact.php">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" name="title" class="form-control" placeholder="Full Name" required />
                    <span class="required">*</span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" name="title" class="form-control" placeholder="Phone" required />
                    <span class="required">*</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <input type="text" name="title" class="form-control" placeholder="Email" required />
                    <span class="required">*</span>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <textarea name="message" id="message" class="form-control" placeholder="Message"></textarea>
                  </div>
                </div>
              </div>
              <a href="javascript:void(0);" class="btn btn-primary">Send Message</a>
            </form>
          </div>
        </div>
      </div>
    </section>


    <!-- FOOTER STARTS -->
       <?php
    include 'footer.php';
    ?>