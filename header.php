<?php
include 'connection/dbconfig.php';
 session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mega Jobs Home page Template</title>

    <!-- Bootstrap -->
    <link href="css/vendors/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Font Awesome for icon fonts -->
    <link href="css/vendors/font-awesome.min.css" rel="stylesheet">
    <!-- Google Font API for Lato and Montserrat font families -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Montserrat:400,700" rel="stylesheet">
    <!-- CSS for slick slider plugin -->
    <link href="css/vendors/slick.css" rel="stylesheet">
    <link href="css/vendors/slick-theme.css" rel="stylesheet">
    <!-- Main Custom CSS file -->
    <link href="css/app.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <header class="navbar-fixed-top" data-spy="affix" data-offset-top="60">
      <!-- MAIN NAVIGATION STARTS -->
      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo1.png" alt="Mega Jobs Logo"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav text-center">
              <li class="dropdown">
                <a href="index.php">HOME </a>
                
              </li>
              <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">JOBS <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <!--<li><a href="jobscompany.php">Jobs By Company</a></li>-->
                  <li><a href="jobsindustry.php">Jobs By Category</a></li>
                  <li><a href="jobslocation.php">Jobs By Location</a></li>
                </ul>
              </li>
			  
 <?php 
 if(isset($_SESSION['seekemail'])){	
    include("check.php");   
?>


    <?php 

    if ($loginst == 1){ ?>

	
	             <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">JOB SEEKER ZONE <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="seeker_settings.php">Upload resume</a></li>
				  
	  </ul>
              </li>
	
<?php } else { ?>


	
              <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">EMPLOYER ZONE <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="employerlogin.php">Post a job</a></li>

                  <li><a href="employerregistration.php">Register</a></li>
                  <li><a href="employerlogin.php">Login</a></li>
		  
                </ul>
              </li>


              <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">JOB SEEKER ZONE <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="seekerlogin.php">Upload resume</a></li>
                  
				  <li><a href="seekerregistration.php">Register</a></li>
                  <li><a href="seekerlogin.php">Login</a></li>
				  
                </ul>
              </li>
		  
			   <?php } ?>
<?php }
else{ 

?>
 <?php 
 
    include("checkem.php");
     if ($loginem == 1){ ?>
	 
	 <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">EMPLOYER ZONE <span class="caret"></span></a>
                <ul class="dropdown-menu">
				
                  <li><a href="employer_postnewjob.php">Post a job</a></li>
				  <li><a href="employer_applicationlist.php">Applied Candidate List</a></li>
                  <li><a href="employer_shortlist.php">Shortlisted Candidates</a></li>
				   <li><a href="searchresume.php">Search Resume</a></li>
	 
	   </ul>
              </li>
			  <?php } else { ?>


	
              <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">EMPLOYER ZONE <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="employerlogin.php">Post a job</a></li>

                  <li><a href="employerregistration.php">Register</a></li>
                  <li><a href="employerlogin.php">Login</a></li>
		  
                </ul>
              </li>


              <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">JOB SEEKER ZONE <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="seekerlogin.php">Upload resume</a></li>
                  
				  <li><a href="seekerregistration.php">Register</a></li>
                  <li><a href="seekerlogin.php">Login</a></li>
				  
                </ul>
              </li>
	 
	 
	 			   <?php } ?>
<?php } ?>



			
 <?php 
 if(isset($_SESSION['seekemail'])){	
    include("check.php");   
?>


    <?php 

    if ($loginst == 1){ ?>
			     <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">MY ACCOUNT <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="seekerprofile.php">My Account</a></li>
                  <li><a href="seeker_settings.php">Settings</a></li>
                  <li><a href="logout.php">Logout</a></li>
                </ul>
              </li>
			   <li class="sign-in pull-right">
                <a href="seekerprofile.php">
                  <small class="hidden-sm">Welcome!</small>
                  <span class="hidden-sm"><?php echo $cust_name ?></span> 
                  <i class="fa fa-user"></i></a>
              </li>
			  
			  <?php } else { ?>
			  
			  
			  
			  
			  
              <li class="sign-in pull-right">
                <a href="login.php">
                  <small class="hidden-sm">Account</small>
                  <span class="hidden-sm">Sign In</span> 
                  <i class="fa fa-user"></i></a>
              </li>
			  
			   <?php } ?>
<?php }
else{ 

?>
 <?php 
 
    include("checkem.php");
     if ($loginem == 1){ ?>
			     <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">MY ACCOUNT <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="employerprofile.php">My Account</a></li>
                  <li><a href="employer_settings.php">Settings</a></li>
				   <li><a href="employer_postedjobs.php#addedjob">Posted Jobs</a></li>
				   <li><a href="employer_postnewjob.php">Post New Job</a></li>
                 
				  <li><a href="logout.php">Logout</a></li>
                </ul>
              </li>
			   <li class="sign-in pull-right">
                <a href="employerprofile.php">
                  <small class="hidden-sm">Welcome!</small>
                  <span class="hidden-sm"><?php echo $comp_name ?></span> 
                  <i class="fa fa-user"></i></a>
              </li>
			  
			  <?php } else { ?>
			  
			  
			  
			  
			  
              <li class="sign-in pull-right">
                <a href="login.php">
                  <small class="hidden-sm">Account</small>
                  <span class="hidden-sm">Sign In</span> 
                  <i class="fa fa-user"></i></a>
              </li>
			  
			   <?php } ?>
<?php } ?>
            </ul>
          </div>
        </div>
      </nav>
      <!-- MAIN NAVIGATION ENDS -->
    </header>