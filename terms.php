<?php
include 'header.php';
include 'connection/dbconfig.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mega Jobs - Job Details</title>

    <!-- Bootstrap -->
    <link href="css/vendors/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Font Awesome for icon fonts -->
    <link href="css/vendors/font-awesome.min.css" rel="stylesheet">
    <!-- Google Font API for Lato and Montserrat font families -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Montserrat:400,700" rel="stylesheet">
    <!-- CSS for slick slider plugin -->
    <link href="css/vendors/slick.css" rel="stylesheet">
    <link href="css/vendors/slick-theme.css" rel="stylesheet">
    <!-- Main Custom CSS file -->
    <link href="css/app.css" rel="stylesheet" type="text/css" />
  </head>

      <!-- BANNER ENDS -->
      <section class="inner-banner padding-bottom-10">
      <!-- BANNER STARTS -->
      <div class="container">
		<h4 style="color: #ffffff">JOIN US & EXPLORE THOUSANDS OF JOBS</h4>
        <div class="row">
          <div class="col-md-12">
            <!-- JOB SEARCH FORM STARTS -->
                        <form action="searchjobs.php" method="POST" class="form-inline">
              <div class="form-group keyword">
                <input type="text" class="form-control" name="jobtitle" placeholder="Enter job title">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control" name="experience" placeholder="Exp (years)">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control"name="location" placeholder="Locations">
              </div>
              <div class="input-group">
                <i class="fa fa-search"></i>
                <input type="submit" name="submit" value="Find">
              </div>
            </form>
            <!-- JOB SEARCH FORM ENDS -->
          </div>
        </div>
		</div>
		</section>
    <section class="aboutus">
      <div class="container">
        <div class="row">
          
            <h3 style="    text-align: center;font-size: 3em;color: deepskyblue;font-weight: 600; margin: 0px 0px 2em 0px;">TERMS OF USE</h3>
	      	     <div class="col-md-8 about-left" style="text-align:justify;">

	      	   	      <h4> Use of the Site </h4>

	      	   	      <p>This website is the property of and operated by Job Caps HR consultancy and is made available to you solely on the Terms and Conditions set out below. For the purposes of these Terms and Conditions "HR Consultancy", "we", "our" and "us" means Job caps HR consultancy.</p>

	      	   	      <p>HR Consultancy may revise the terms and conditions of use at any time and update this information. You should therefore verify this page regularly and review the Terms and Conditions of use.</p>

	      	   	      </br></br>

	      	   	      <h4>Intellectual property</h4>

	      	   	      <p>HR Consultancy is the owner and/or authorised copyright licence holder of the logos, trademarks, images and any other content present on hrconsultancy.co.uk. By posting this information on our website we do not grant any licence, copyright or use of any other of our intellectual property rights to any third party.</p></br></br>

<h4>Data protection</h4>

<p>The use of Curriculum Vitae, phone numbers, email addresses, personal addresses and any other personal information supplied to us by you and other users of this website is governed by our "Privacy Policy". By accepting the Terms and Conditions, you expressly consent to the disclosures of personal data to third parties and to Job Caps  HR Consultancy's use of information as detailed in the Privacy Policy.</p>

<p>You should note that certain URLs on the site link to other websites which are not under the control of Job Caps HR Consultancy. We accept no responsibility or liability for any material on any website that is not under the control of Job Caps HR Consultancy.</p>

<p>We make no warranty that the contents of this website are free from the influence of malware which has destructive or monitoring properties and shall have no liability in respect thereof.</p></br></br>

<h4>Governing Law</h4>

<p>The Terms & Conditions of use are governed and shall be construed in accordance with the law of India and each party submits to the exclusive jurisdiction of Indian/kerala courts.</p>

	      	     </div>      
          
	      	     <div class="col-md-4 about-right">

	      	   	       <img src="images/terms.jpg" alt="">

	      	     </div>

	      	   <div class="clearfix"> </div>
				 </div>
        </div>
      </div>
    </section>



    <!-- FOOTER STARTS -->
       <?php
    include 'footer.php';
    ?>