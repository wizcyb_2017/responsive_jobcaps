<?php
include 'header.php';
include 'connection/dbconfig.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mega Jobs - Job Details</title>

    <!-- Bootstrap -->
    <link href="css/vendors/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Font Awesome for icon fonts -->
    <link href="css/vendors/font-awesome.min.css" rel="stylesheet">
    <!-- Google Font API for Lato and Montserrat font families -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Montserrat:400,700" rel="stylesheet">
    <!-- CSS for slick slider plugin -->
    <link href="css/vendors/slick.css" rel="stylesheet">
    <link href="css/vendors/slick-theme.css" rel="stylesheet">
    <!-- Main Custom CSS file -->
    <link href="css/app.css" rel="stylesheet" type="text/css" />
  </head>

      <!-- BANNER ENDS -->
      <section class="inner-banner padding-bottom-10">
      <!-- BANNER STARTS -->
      <div class="container">
		<h4 style="color: #ffffff">JOIN US & EXPLORE THOUSANDS OF JOBS</h4>
        <div class="row">
          <div class="col-md-12">
            <!-- JOB SEARCH FORM STARTS -->
                        <form action="searchjobs.php" method="POST" class="form-inline">
              <div class="form-group keyword">
                <input type="text" class="form-control" name="jobtitle" placeholder="Enter job title">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control" name="experience" placeholder="Exp (years)">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control"name="location" placeholder="Locations">
              </div>
              <div class="input-group">
                <i class="fa fa-search"></i>
                <input type="submit" name="submit" value="Find">
              </div>
            </form>
            <!-- JOB SEARCH FORM ENDS -->
          </div>
        </div>
		</div>
		</section>
    <section class="aboutus">
      <div class="container">
        <div class="row">
          
            <h3 style="    text-align: center;font-size: 3em;color: deepskyblue;font-weight: 600; margin: 0px 0px 2em 0px;">Privacy</h3>
	      	     <div class="col-md-8 about-left" style="text-align:justify;" >

	      	   	      <h4> Privacy Commitment</h4>

	      	   	      <p>This Privacy Policy describes the information that we collect about you when you use our website.</p>

	      	   	      <p >We are committed to protecting your privacy when you use our on-line. In this policy we explain how and why we collect your information, what we do with it and what controls you have over our use of it.</p>

	      	   	      <p>Please read this Policy before using the Site or submitting any personal information (if any).</p>

	      	   	      <p>By using the Site, you are accepting the practices described in this Policy. These practices may be changed, but any changes will be posted and such changes will apply only on prospective basis. You are encouraged to review the Policy whenever you visit the Site to make sure that you understand how any personal information you provide will be used.</p></br></br>

	      	   	      <h4>Collection Of Information</h4>

	      	   	      <p>Jobcaps collects and processes information about you in order to:</p>

	      	   	      <p>(i) identify you each time you visit a Web Site or wish to have a Service provided;</p>

<p>(ii) process postings or applications submitted by you;</p>

<p>(iii) improve our Services and Web Sites</p>

<p>(iv) Customise your experience, for example, provide advertisements that we think are relevant to you and which support any specific requests for information you may make through keyword searches;</p>

<p>(v) carry out research on the demographics, interests and behaviour of all of our customers; and</p>

<p>(vi) send you information we think you may find useful, including information about new jobcaps products and services.

</p><p>You may be asked, either when you register with us, or at other times, for information about yourself, such as your name, e-mail address, postal address and telephone number. You may also be asked to share with us your interests, hobbies and preferences. In addition, when you order certain goods or services from our Web Sites, we will need to know your credit/payment card number and expiration date.

You are under no obligation to provide this information, but if you don’t then we may not be able to provide you with certain services or personalise your experience and tailor our Services for you (for example, tell you about special offers on things you’re interested in).

</p></br></br>

<h4>Use and Retention of your information</h4>

<p>Use of Personal Information: We use your personal information for such purposes as responding to your requests; monitoring and providing you with content that we believe would be of interest to you. We may also email newsletters, special editions and similar communications about the Site. We never share mailing lists with any third parties, including advertisers and partners. We will not share information with any third party, except to comply with applicable law or valid legal process.</p>

<p>Use of Other Information: We use other information for purposes such as measuring the number of visitors to sections of our Site, making the Site more effective and useful to visitors.</p>

<p>Retention of Information: We retain information for as long as required, allowed or we believe it useful, but do not undertake retention obligations. We may dispose of information in our discretion without notice, subject to applicable law that specifically requires the handling or retention of information.</p>

<p>Access to the Information: We make good faith efforts to provide you with access to your personal information and either to correct the data if it is inaccurate or to delete such data at your request if it is not otherwise required to be retained by law or for legitimate business purposes.</p>

<p>Commitment to Data Security: Your personally identifiable information is kept secure and shall be used only for the intended purpose.</p>

<p>Opting Out: You have the option of not to receive our electronic newsletters.

</p>

	      	     </div>       
          
	      	     <div class="col-md-4 about-right">

	      	   	       <img src="images/privacy-policy.jpg" alt="">

	      	     </div>
				 </div>
        </div>
      </div>
    </section>



    <!-- FOOTER STARTS -->
       <?php
    include 'footer.php';
    ?>