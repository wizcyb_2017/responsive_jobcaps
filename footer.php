<!-- FOOTER STARTS -->
    <footer>
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-lg-offset-1 col-md-3 col-sm-6">
            <h5 style="font-size: 18px"><strong>BROWSE JOBS</strong></h5>
            <ul class="quick-links">
              <li><a href="index.php">Browse All Jobs</a></li>
             <!-- <li><a href="jobscompany.php">Jobs by Company</a></li>-->
              <li><a href="jobsindustry.php">Jobs by Category</a></li>
              <li><a href="jobslocation.php">Jobs by Location</a></li>

            </ul>
          </div>
         
			
			
			
			 <?php 
 if(isset($_SESSION['seekemail'])){	
    include("check.php");   
?>


    <?php 

    if ($loginst == 1){ ?>
	 <div class=" col-md-3 col-sm-6">
            <h5 style="font-size: 18px"><strong>JOBSEEKERS</strong></h5>
	<ul class="quick-links">
	<li><a href="seeker_settings.php">Upload resume</a></li>
	</ul>
	</div>
	<?php } else { ?>
	
	
	
	 <div class="col-lg-2 col-md-3 col-sm-6">
            <h5 style="font-size: 18px"><strong>JOBSEEKERS</strong></h5>
            <ul class="quick-links">
              <li><a href="seekerregistration.php">Register Now</a></li>
              <li><a href="seekerlogin.php">Login</a></li>
              <li><a href="reportbug.php">Report a Problem</a></li>

            </ul>
          </div>
          <div class="col-lg-2 col-md-3 col-sm-6">
            <h5 style="font-size: 18px"><strong>EMPLOYERS<strong></h5>
            <ul class="quick-links">
              <li><a href="employerregistration.php">Register Now</a></li>
              <li><a href="employerlogin.php">Login</a></li>
              <li><a href="login.php">Post Jobs</a></li>			  
              <li><a href="reportbug.php">Report a Problem</a></li>

            </ul>
          </div>
		  
		  			   <?php } ?>
<?php }
else{ 

?>
 <?php 
 
    include("checkem.php");
     if ($loginem == 1){ ?>
	 
	           <div class=" col-md-3 col-sm-6">
            <h5 style="font-size: 18px"><strong>EMPLOYERS<strong></h5>
            <ul class="quick-links">
             <li><a href="employer_postnewjob.php">Post a job</a></li>
				  <li><a href="employer_applicationlist.php">Applied Candidate List</a></li>
                  <li><a href="employer_shortlist.php">Shortlisted Candidates</a></li>
				   <li><a href="searchresume.php">Search Resume</a></li>

            </ul>
          </div>
	  <?php } else { ?>
	 	 <div class="col-lg-2 col-md-3 col-sm-6">
            <h5 style="font-size: 18px"><strong>JOBSEEKERS</strong></h5>
            <ul class="quick-links">
              <li><a href="seekerregistration.php">Register Now</a></li>
              <li><a href="seekerlogin.php">Login</a></li>
              <li><a href="reportbug.php">Report a Problem</a></li>

            </ul>
          </div>
          <div class="col-lg-2 col-md-3 col-sm-6">
            <h5 style="font-size: 18px"><strong>EMPLOYERS<strong></h5>
            <ul class="quick-links">
              <li><a href="employerregistration.php">Register Now</a></li>
              <li><a href="employerlogin.php">Login</a></li>
              <li><a href="login.php">Post Jobs</a></li>			  
              <li><a href="reportbug.php">Report a Problem</a></li>

            </ul>
          </div>
		  	 			   <?php } ?>
<?php } ?>


	 
	 
	 
	 
	 
          <div class="col-lg-2 col-md-3 col-sm-6">
            <h5 style="font-size: 18px"><strong>ABOUT OUR SITE <strong></h5>
            <ul class="quick-links">
              <li><a href="contactus.php">Contact Us</a></li>
              <li><a href="aboutus.php">About Us</a></li>
			  <li><a href="privacy.php">Privacy Commitment</a></li>
			  <li><a href="terms.php">Terms of Use</a></li>
              <li><a href="reportbug.php">Report a Bug/Abuse</a></li>

            </ul>
          </div>		  
        </div>
        <div class="copyright">
          <div class="row">
            <div class="col-sm-6">
			
               <a href="mailto:mail@jobcaps.com">jobcaps hr consultancy</a> | 2017 © powered by <a href="http://wizcyb.com/">wizcyb interactive</a>
            </div>
			
            <div class="col-sm-6 author-info">

			<ul class="social-links" >
			
              <li>
                <a href="#" title="Facebook" class="fa fa-facebook"></a>
              </li>
              <li>
                <a href="#" title="Twitter" class="fa fa-twitter"></a>
              </li>
              <li>
                <a href="#" title="Linkedin" class="fa fa-linkedin"></a>
              </li>
              <li>
                <a href="#" title="Google+" class="fa fa-google-plus"></a>
              </li>
            </ul>
             
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- FOOTER ENDS -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Bootstrap JS -->
    <script src="js/vendors/bootstrap.min.js"></script>
    <!-- Slick slider plugin JS -->
    <script src="js/vendors/slick.min.js"></script>
    <!-- Countdown plugin used on coming soon page -->
    <script src="js/vendors/jquery.countdown.min.js"></script>
    <!-- Summernote Text editor plugin used on create resume page -->
    <script src="js/vendors/summernote.min.js"></script>
    <!-- Custom Javascript code as per requirement -->
    <script src="js/custom.js"></script>
  </body>
</html>