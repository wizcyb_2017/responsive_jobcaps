<?php
error_reporting(0);
  include "dbconfig.php";
                   
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>JOB CAPS| Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

    <?php require('mainheader.php'); ?>
     
     <?php require('sidebar.php'); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-user"></i></span>
                <div class="info-box-content">

                 
                <?php
$result=mysqli_query($con,"SELECT COUNT(*) AS total FROM employer where em_status='1'");
$row= mysqli_fetch_assoc($result);
 ?>
             <span class="info-box-text">TOTAL EMPLOYERS </span>
                  <span class="info-box-number"><?php echo $row['total']; ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content"> 
               
                <?php
$result=mysqli_query($con,"SELECT COUNT(*) AS total FROM seeker where seek_status='1'");
$row= mysqli_fetch_assoc($result);
 ?>

                      
                          
                  <span class="info-box-text">TOTAL JOBSEEKER </span>
                  <span class="info-box-number"><?php echo $row['total']; ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="glyphicon glyphicon-lock"></i></span>
                <div class="info-box-content">
                 <?php
$result=mysqli_query($con,"SELECT COUNT(*) AS total FROM jobs where status='1'");
$row= mysqli_fetch_assoc($result);
 ?>
                      
                       
              <span class="info-box-text">TOTAL JOB POSTED </span>
                  <span class="info-box-number"><?php echo $row['total']; ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
           
          </div><!-- /.row -->

         

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
              <!-- MAP & BOX PANE -->
              
              <div class="row">
                  
                <div class="col-md-6">
                  <!-- USERS LIST -->
                  <div class="box box-danger">
                   
                    <div class="box-header with-border">
                      <h3 class="box-title">Pending Jobseeker Request</h3>
                      
                      <div class="box-tools pull-right">
                        <?php
$result=mysqli_query($con,"SELECT COUNT(*) AS total FROM seeker where seek_status='0'");
$row= mysqli_fetch_assoc($result);
 ?>
                      
                      
                      
                        <span class="label label-danger"><?php echo $row['total']; ?> New Members</span>

                        <?php

                      
                  
              //$query2 = mysqli_query($con,"SELECT * FROM registration ORDER BY name ASC") or die("insert query error!".mysqli_error());
              $ros=mysqli_query($con,"SELECT * FROM seeker where seek_status='0'");
              if(!$ros)
              {
                  die('Could not get data: ' . mysql_error());
              }
            ?>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                      <ul class="users-list clearfix">
                       <?php
                          while($row = mysqli_fetch_array($ros, MYSQL_ASSOC))
                    {

                   
              ?>
                        <li>
                      <?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['content']).'"/>' ?>
                          <a class="users-list-name" href="#"><?php echo $row['seekname'];?></a>
                          <span class="users-list-date"><?php echo $row['qualification'];?></span>
                        </li>
                        <?php
                              }
                            ?>
                       
                      </ul><!-- /.users-list -->
                    </div><!-- /.box-body -->
                    <div class="box-footer text-center">
                      <a href="seekerrequest.php" class="uppercase">View All Users</a>
                    </div><!-- /.box-footer -->
                  </div><!--/.box -->
                </div><!-- /.col -->

                <div class="col-md-6">
                  <!-- USERS LIST -->
                  <div class="box box-danger">
                   
                    <div class="box-header with-border">
                      <h3 class="box-title">Pending Employer Request</h3>
                      
                      <div class="box-tools pull-right">
                       <?php
$result=mysqli_query($con,"SELECT COUNT(*) AS total FROM employer where em_status='0'");
$row= mysqli_fetch_assoc($result);
 ?>
                      
                      
                      
                        <span class="label label-danger"><?php echo $row['total']; ?> New Members</span>

                        <?php
                  
              //$query2 = mysqli_query($con,"SELECT * FROM registration ORDER BY name ASC") or die("insert query error!".mysqli_error());
              $ros=mysqli_query($con,"SELECT * FROM employer where em_status='0'");
              if(!$ros)
              {
                  die('Could not get data: ' . mysql_error());
              }
            ?>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                      <ul class="users-list clearfix">
                       <?php
                          while($row = mysqli_fetch_array($ros, MYSQL_ASSOC))
                    {

                   
              ?>
                        <li>
                        <?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['content']).'" width="128px" height="128px"/>' ?>
                         
                          <a class="users-list-name" href="#"><?php echo $row['companyname'];?></a>
                          <span class="users-list-date"><?php echo $row['companytype'];?></span>
                        </li>
                        <?php
                              }
                            ?>
                       
                      </ul><!-- /.users-list -->
                    </div><!-- /.box-body -->
                    <div class="box-footer text-center">
                      <a href="employerrequest.php" class="uppercase">View All Users</a>
                    </div><!-- /.box-footer -->
                  </div><!--/.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->

         
            </div><!-- /.col -->

            <div class="col-md-4">
              <!-- Info Boxes Style 2 -->
                       <div class="box box-primary">
                <div class="box-header with-border">
                <?php
                   $result=mysqli_query($con,"SELECT COUNT(*) AS total FROM jobs where status='0'");
                   $row= mysqli_fetch_assoc($result);
                   ?>
                      
                       
                      
                       
                  <h3 class="box-title">Pending Posted Job request</h3>
                   <span class="label label-danger"> <?php echo $row['total']; ?>  New Job Request</span>
                   
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
                  <?php
              //$query2 = mysqli_query($con,"SELECT * FROM registration ORDER BY name ASC") or die("insert query error!".mysqli_error());
              $ros=mysqli_query($con,"SELECT * FROM jobs where status='0'");
              if(!$ros)
              {
                  die('Could not get data: ' . mysql_error());
              }
         
                          while($row = mysqli_fetch_array($ros, MYSQL_ASSOC))
                    {

                   
              ?>
              
                    <li class="item">
                      <div class="product-img">
                        <img src="dist/img/default-50x50.gif" alt="Product Image">
                      </div>
                      <div class="product-info">
                        <a href="javascript::;" class="product-title"><?php echo $row['jobtitle'];?> <span class="label label-warning pull-right">Last Date &nbsp;<?php echo $row['lastdate'];?></span></a>
                        <span class="product-description">
                          Company Name:<?php echo $row['companyname'];?></br>
                          Location:<?php echo $row['location'];?></br>
                          Qualificaton:<?php echo $row['qualification'];?></br>
                          Date Posted:<?php echo $row['posteddate'];?>
                        </span>
                      </div>
                    </li><!-- /.item -->
                    <?php
                              }
                            ?>
                   
                    
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="jobrequest.php" class="uppercase">View All Products</a>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
              
            
              
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <?php require "footer.php"; ?>

      <!-- Control Sidebar -->
       <?php include"controlsidebar.php"; ?><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard2.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
  </body>
</html>
