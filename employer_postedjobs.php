<?php  
error_reporting(0);
session_start();  
include 'header.php';

  
  

if(!$_SESSION['email'])  

{  

  

    header("Location: login.php");//redirect to login page to secure the welcome page without login access.  

}  

  



include 'connection/dbconfig.php';

$companyname="";
$content="";
$password="";
$email="";
$contactperson="";
$designation="";
$companytype="";
$indtype="";
$webadrress="";
$adrress="";
$country="";
$state="";
$district="";
$tele="";
$mobile="";
$name='';
$type='';
$size='';
$em_id='';


//echo $_SESSION['email'];  

 
if(isset($_SESSION['email']))
{
    $id = $_SESSION['email'];
    $selquery = mysqli_query($con,"SELECT * FROM employer where email='$id' ");
    while($row = mysqli_fetch_array($selquery))
    {
       
        
        $em_id= $row['em_id'];
        $name= $row['name'];
        $type= $row['type'];  
        $size= $row['size'];
        $content= $row['content'];
        $companyname=$row['companyname'];
        $password=$row['empassword'];
        $email=$row['email'];
        $contactperson=$row['contactperson'];
        $companytype=$row['companytype'];
        $designation=$row['designation'];
        $indtype=$row['indtype'];
        $webadrress=$row['webadrress'];
        $adrress=$row['adrress'];
        $country=$row['country'];
        $state=$row['state'];
        $district=$row['district'];
        $tele=$row['tele'];
        $mobile=$row['mobile'];


        
       
    }
    
}
?>


    <section class="inner-banner">
      <!-- BANNER STARTS -->
      <div class="container">
        <h4 style="color: #ffffff">JOIN US & EXPLORE THOUSANDS OF JOBS</h4>
        <div class="row">
          <div class="col-md-12">
            <!-- JOB SEARCH FORM STARTS -->
            <form action="searchjobs.php" method="POST" class="form-inline">
              <div class="form-group keyword">
                <input type="text" class="form-control" name="jobtitle" placeholder="Enter job title">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control" name="experience" placeholder="Exp (years)">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control"name="location" placeholder="Locations">
              </div>
              <div class="input-group">
                <i class="fa fa-search"></i>
                <input type="submit" name="submit" value="Find">
              </div>
            </form>
            <!-- JOB SEARCH FORM ENDS -->
          </div>
        </div>
      </div>
      <!-- BANNER ENDS -->
    </section>


    <section class="">
      <div class="container">
        <div class="row">
          <div class="sidebar col-md-3 col-md-offset-0 col-sm-4 col-sm-offset-3">
            <div class="user-intro">
              <img src="images/profilebg.jpg" alt="User profile wall">
                <div class="candidate-info">
			  <div >
               <?php echo '<img   height="200px" weight="54px" alt="User profile picture"  class="img-circle"  src="data:image/jpeg;base64,'.base64_encode($content).'"   /> ' ?> 
				</div>
                <h4 style="text-transform: capitalize;"><?php echo $companyname; ?></h4>
                <p><?php echo $contactperson ?></p>
				<p><?php echo $designation ?></p>

				    <ul class="social-links">
                  <li>
                    <a href="#" title="Facebook" class="fa fa-facebook"></a>
                  </li>
                  <li>
                    <a href="#" title="Twitter" class="fa fa-twitter"></a>
                  </li>
                  <li>
                    <a href="#" title="Linkedin" class="fa fa-linkedin"></a>
                  </li>
                  <li>
                    <a href="#" title="Google+" class="fa fa-google-plus"></a>
                  </li>
                </ul>
              </div>
            </div>

          </div>
          <div class="col-md-9 col-sm-9">
            
			
	
            <div class="divider"></div>
			  <div class="box"> 
            <h4>Post New Jobs <span class="small pull-right" style="text-transform: capitalize;">Fields with * are mandetory</span></h4><br></br>
            
            
                <div class="box-body"> 
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       
                        <th>Job id</th>
                       <th>Job Title</th>
                        <th>Posted Date</th>
                       <th>lastdate</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    
		 <?php
		        $id = $_SESSION['email'];

                    define('MAX_REC_PER_PAGE', 7);
                    $c=1;
                    $rs = mysqli_query($con,"SELECT COUNT(*) FROM jobs WHERE status='1' AND em_emailid='$id'") or die("Count query error!");
                    list($total) = mysqli_fetch_row($rs);
                    $total_pages = ceil($total / MAX_REC_PER_PAGE);
                    $page = intval(@$_GET["page"]);     
                    if (0 == $page){
                    $page = 1;
                    }       
                    $start = MAX_REC_PER_PAGE * ($page - 1);
                    $max = MAX_REC_PER_PAGE;
                    $ros=mysqli_query($con,"SELECT * FROM jobs WHERE status='1' AND em_emailid='$id' ORDER BY id DESC LIMIT $start,$max ") or die("select product error!".mysqli_error());
                     while($row = mysqli_fetch_array($ros, MYSQL_ASSOC))
                    {
        
                    
                ?>	
                      <tr>
                      <td><?php echo $row['job_id'];?></td>
                        <td><?php echo $row['jobtitle'];?></td>
                        <td><?php echo $row['posteddate'];?></td>
                        <td><?php echo $row['lastdate'];?></td>
                         <td><?php 
                         if($row['status']==1)
                              {
                                echo "<span class='label label-success'>Approved</span>";
                                } 
                                else{
                                  echo "<span class='label label-warning'>Pending</span>";
                                } ?></td>
                       <td class="center">
           <a class="btn btn-success " href="jobsingle.php?id=<?php echo $row['id']; ?>">
                <i class="glyphicon glyphicon-zoom-in icon-white"></i>
                View
            </a>
           <a class="btn btn-danger" href="jobdelete.php?id=<?php echo $row['id']; ?>">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
            </a>

            <a class="btn btn-warning" href="jobsingleedit.php?id=<?php echo $row['id']; ?>">
                    <i class="glyphicon glyphicon-edit icon-white"></i> Edit
                  </a>
            </td></tr>
                       <?php $c++;
            } 
            if($total==0) {
            ?>
            <tr>
                <td colspan="6" class="norec">No records found!</td>
            </tr>
			
            <?php }  ?>
			
                   </tbody> 
                  </table>
				<?php 
			if($total!=0) {
?>				
				  <div>
				  
              <ul class="pagination pull-right">
                           
                                
                                <?php
                                /*echo "Go to page";*/
                                for ($i = 1; $i <= $total_pages; $i++) {
                                $txt = $i;
                                if ($page != $i)
                                //$txt = "<li><a href='" . $_SERVER["PHP_SELF"] . "?page=$i&id=".$_GET['id']."' class=\"paging_in\">$txt</a></li>";
                                $txt = "<li><a href=\"" . $_SERVER["PHP_SELF"] . "?page=$i\" class=\"paging_in\">$txt</a></li>";
                                ?>          
                                <li>
                                <?php echo $txt; ?></li>
                                <?php
                                }
                                ?>
                            
                        
            </ul>
          
			
				  </div>
				  <?php 
				  
			}
				  ?>
                </div><!-- /.box-body -->
              </div>
          </div>
        </div>
		               
      </div>
    </section>
    <!-- FOOTER STARTS -->
       <?php
    include 'footer.php';
    ?>
	    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
<?php
                $sql ="SELECT jobs FROM registration order by regno desc limit 0,1";
              $result = mysqli_query($con, $sql);
              if($result && mysqli_num_rows($result) > 0)
              {


                while($data=mysqli_fetch_array($result))
                {
                           
              ?>  
              
                  <input type="text" class="span3 typeahead"  name="regno" id="regno" value="<?php echo $data['regno'];?>"/>            
                  
            <?php
          }} ?>
		  
<?php 
 
if(isset($_POST['submit'])){
                 
                  
                  $companyname=$_POST['companyname'];
                  $job_id=$_POST['job_id'];
                  $em_emailid=$_POST['email'];
                  $jobtitle=$_POST['jobtitle'];
                  $posteddate=date("d-M-Y");
                  $lastdate=$_POST['lastdate'];
                  $companytype=$_POST['companytype'];
                  $companyprofile =$_POST['indtype'];
                  $webadrress=$_POST['webadrress'];
                  $location=$_POST['location'];
                  $catagory=$_POST['catagory'];
                  $position=$_POST['position'];
                  $qualification=$_POST['qualification'];
                  $experience=$_POST['experience'];
                  $keyskills=$_POST['keyskills'];
                  $description=$_POST['description'];
                  $address=$_POST['address'];
    
    $insquery= mysqli_query($con,"INSERT INTO jobs (job_id,em_emailid,jobtitle,posteddate,companyname,companyprofile,companytype,webadrress,location,catagory,position,qualification,experience,keyskills,description,lastdate,address) VALUES ('$job_id','$em_emailid','$jobtitle','$posteddate','$companyname','$companyprofile','$companytype','$webadrress','$location','$catagory','$position','$qualification','$experience','$keyskills','$description','$lastdate','$address')");

        if($insquery){
        ?>
        <script>alert('You Successfully posted Job');
        window.location = 'employer_postedjobs.php';
        </script>
        <?php
        }
        else
        {
        ?>
        <script>alert('Ooops! Some problem occured .Try Again ');
        window.location = 'employer_postedjobs.php';
        </script>
        <?php 
        }
}
?>