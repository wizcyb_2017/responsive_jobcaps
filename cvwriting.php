<?php
include 'header.php';
include 'connection/dbconfig.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mega Jobs - Job Details</title>

    <!-- Bootstrap -->
    <link href="css/vendors/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Font Awesome for icon fonts -->
    <link href="css/vendors/font-awesome.min.css" rel="stylesheet">
    <!-- Google Font API for Lato and Montserrat font families -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Montserrat:400,700" rel="stylesheet">
    <!-- CSS for slick slider plugin -->
    <link href="css/vendors/slick.css" rel="stylesheet">
    <link href="css/vendors/slick-theme.css" rel="stylesheet">
    <!-- Main Custom CSS file -->
    <link href="css/app.css" rel="stylesheet" type="text/css" />
  </head>

      <!-- BANNER ENDS -->
      <section class="inner-banner padding-bottom-10">
      <!-- BANNER STARTS -->
      <div class="container">
		<h4 style="color: #ffffff">JOIN US & EXPLORE THOUSANDS OF JOBS</h4>
        <div class="row">
          <div class="col-md-12">
            <!-- JOB SEARCH FORM STARTS -->
                        <form action="searchjobs.php" method="POST" class="form-inline">
              <div class="form-group keyword">
                <input type="text" class="form-control" name="jobtitle" placeholder="Enter job title">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control" name="experience" placeholder="Exp (years)">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control"name="location" placeholder="Locations">
              </div>
              <div class="input-group">
                <i class="fa fa-search"></i>
                <input type="submit" name="submit" value="Find">
              </div>
            </form>
            <!-- JOB SEARCH FORM ENDS -->
          </div>
        </div>
		</div>
		</section>
    <section class="aboutus">
      <div class="container">
        <div class="row">
          
            <h3 style="    text-align: center;font-size: 3em;color: deepskyblue;font-weight: 600; margin: 0px 0px 2em 0px;">PROFESSIONAL CV WRITING SERVICE</h3>
	      	     <div class="col-md-8 about-left" style="text-align:justify;">

	      	   	    

	      	   	     	      	   	      <p>The CV plays an important role in job hunt. A well-presented CV helps employer to know who you are and what you can offer them. It gives a clear insight on you and your past and potential abilities. In today’s job market, standing out from the crowd is essential and having a tailor-made professional CV will set you head and shoulders above your competition. Unlike the resume, the CV is more detailed and needs to be described in a specific way. If you have the correct CV you can use it to target several employers who are looking to know more about you than just what is specifically related to their job. Having a professionally written CV is the best way to make an impact and get employers to notice you!</p>

	      	   	      <p>Our talent team is to help you to sell your talents. They know exactly what employers are looking for as they have extensive experience of writing CVs in all industries that you are applying for. Drive your career now with our talented writers.</p>


	      	   	      </br></br>

	      	   	      <h3>Career Starter CV</h3>

	      	   	      <p>0-2 years  experience</br>
                         Price Includes: A Starter CV / Resume, Cover Letter </br>
                         Price : Rs 900/-</p></br></br>

                         <h3>Mid Career CV</h3>

                         <p>2-10  years  experience</br>
                        Price Includes: An Intermediate CV / Resume, Cover Letter and LinkedIn Profile,</br>
                         Price : Rs 1600/-</p></br></br>

                          <h3>Senior Career CV</h3>
                          <p>Above 10  years  experience</br>
                          Price Includes: An Intermediate CV / Resume, Cover Letter and LinkedIn Profile,</br> 
                          Price : Rs 2400/-</p></br></br>
                          
                          <h3>Infographic  CV</h3>
                           <p> Our specially designed Infographic CVs combine the powerful features of photoshop with our crisp writing style to go far beyond the way we perceive CVs to be.</br>
                          Price : Rs 2900/-</p></br></br>
                          
                          <h3>Website CV</h3>
                           <p> Website CV is also known as an online Curriculum Vitae (CV). This one page website replaces the traditional paper format with a more impressive digital presentation.</p>
                          <p>The Price includes CV/resume website design, CV/resume writing, domain name and hosting.</br>
                          Price : Rs  5900/-</p></br></br>
	      	     </div>      
          
	      	     <div class="col-md-4 about-right">

	      	   	       <img src="images/cvwriting.jpg" alt="">

	      	     </div>

	      	   <div class="clearfix"> </div>
				 </div>
        </div>
      </div>
    </section>



    <!-- FOOTER STARTS -->
       <?php
    include 'footer.php';
    ?>