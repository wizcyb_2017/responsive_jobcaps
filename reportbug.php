<?php
include 'header.php';
include 'connection/dbconfig.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mega Jobs - Job Details</title>

    <!-- Bootstrap -->
    <link href="css/vendors/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Font Awesome for icon fonts -->
    <link href="css/vendors/font-awesome.min.css" rel="stylesheet">
    <!-- Google Font API for Lato and Montserrat font families -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Montserrat:400,700" rel="stylesheet">
    <!-- CSS for slick slider plugin -->
    <link href="css/vendors/slick.css" rel="stylesheet">
    <link href="css/vendors/slick-theme.css" rel="stylesheet">
    <!-- Main Custom CSS file -->
    <link href="css/app.css" rel="stylesheet" type="text/css" />
  </head>

      <!-- BANNER ENDS -->
      <section class="inner-banner padding-bottom-10">
      <!-- BANNER STARTS -->
      <div class="container">
		<h4 style="color: #ffffff">JOIN US & EXPLORE THOUSANDS OF JOBS</h4>
        <div class="row">
          <div class="col-md-12">
            <!-- JOB SEARCH FORM STARTS -->
                        <form action="searchjobs.php" method="POST" class="form-inline">
              <div class="form-group keyword">
                <input type="text" class="form-control" name="jobtitle" placeholder="Enter job title">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control" name="experience" placeholder="Exp (years)">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control"name="location" placeholder="Locations">
              </div>
              <div class="input-group">
                <i class="fa fa-search"></i>
                <input type="submit" name="submit" value="Find">
              </div>
            </form>
            <!-- JOB SEARCH FORM ENDS -->
          </div>
        </div>
		</div>
		</section>

    <section class="aboutus">
	<h2 style="    color: #55d3e1;"> <center><b>Report bug </b>  </center></h2>
      <div class="container">
        <div class="row">

          <div class="col-md-12">
            
            <form method="post" action="report.php">
              <div class="row">
			  <div class="col-sm-10">
			  
			  
			  						 <div class="form-group">
									 <label>Subject:</label>
					   <select class="form-control" name="subject" id="gender" class="form-control">
                    <option selected="selected" value="">- Company Type -</option>
                        <option value="Problems with the Site" selected="">Problems with the Site</option>

                        <option value="Request Information">Request Information</option>

                        <option value="General Feedback">General Feedback</option>

                        <option value="Report Abuse">Report Abuse</option>
                    
					</select>
					   
					</div>
			  
			  
			  
			  
			  

			  </div>
			  
			  
			  
                <div class="col-sm-10">
                  <div class="form-group">
				  <label>Name:</label>
                    <input type="text" name="title" class="form-control" placeholder="Full Name" required />
                    <span class="required">*</span>
                  </div>
                </div>
                <div class="col-sm-10">
                  <div class="form-group">
				  <label>Phone Number:</label>
                    <input type="text" name="title" class="form-control" placeholder="Phone" required />
                    <span class="required">*</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-10">
                  <div class="form-group">
				  <label>Email:</label>
                    <input type="text" name="title" class="form-control" placeholder="Email" required />
                    <span class="required">*</span>
                  </div>
                </div>
                <div class="col-sm-10">
                  <div class="form-group">
				  <label>Message:</label>
                    <textarea name="message" id="message" class="form-control" placeholder="Message"></textarea>
                  </div>
                </div>
              </div>
              <a href="javascript:void(0);" class="btn btn-primary">Send Message</a>
            </form>
          </div>
        </div>
      </div>
    </section>


    <!-- FOOTER STARTS -->
       <?php
    include 'footer.php';
    ?>