<?php
include 'header.php';
include 'connection/dbconfig.php';


 ?>

    <section class="inner-banner">
      <!-- BANNER STARTS -->
      <div class="container">
        <h4 style="color: #ffffff">JOIN US & EXPLORE THOUSANDS OF JOBS</h4>
        <div class="row">
          <div class="col-md-12">
            <!-- JOB SEARCH FORM STARTS -->
            <form action="searchjobs.php" method="POST" class="form-inline">
              <div class="form-group keyword">
                <input type="text" class="form-control" name="jobtitle" placeholder="Enter job title">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control" name="experience" placeholder="Exp (years)">
              </div>
              <div class="form-group keyword hidden-xs">
                <input type="text" class="form-control"name="location" placeholder="Locations">
              </div>
              <div class="input-group">
                <i class="fa fa-search"></i>
                <input type="submit" name="submit" value="Find">
              </div>
            </form>
            <!-- JOB SEARCH FORM ENDS -->
          </div>
        </div>
      </div>
      <!-- BANNER ENDS -->
    </section>
    <section class="search-result">
      <div class="container">
        <div class="row">
          <div class="sidebar col-md-12 col-sm-8">
           <!-- <h5>Refine Search <a href="javascript:void(0);" class="pull-right">Clear All Filter</a></h5>-->
			
			
            <div style="background-color: rgb(52, 73, 94); height:auto; display:block; margin-top:10px; margin-bottom:10px; padding:10px; overflow: hidden;">

            <div style="background-color: rgb(52, 73, 94); height:auto; display:block; margin-top:10px; margin-bottom:10px; padding:10px; overflow: hidden;">

  <div style="padding: 20px;"  id="form-olvidado">
    <h4 class="" style="color:white">
      Forgot your password?
    </h4>
    <form  method="post" action="forgotpasswordseek.php">
      <fieldset>
        <span class="help-block" style="color:white">
          Email address you use to log in to your account
          <br>
          We'll send you an email with instructions to choose a new password.
        </span>
        <div class="form-group input-group">
          <span class="input-group-addon">
            @
          </span>
          <input class="form-control" placeholder="Email" name="seekemail" type="email" required="">
        </div>
        <button type="submit" class="btn btn-primary btn-block"  name="forgotpasswordseek">
          Continue
        </button>
        <p class="help-block">
          <a class="text-muted" href="seekerlogin.php" style="color:white"><small>Account Access</small></a>
        </p>
      </fieldset>
    </form>
  </div>

            </div>

            </div>
			
			
		
				
			


          </div>
		  
		  
		   
 
            <!-- <ul class="pagination pull-right">
              <li><a href="javascript:void(0);">1</a></li>
              <li><a href="javascript:void(0);">2</a></li>
            </ul> -->
          </div>
        </div>
      </div>
    </section>
    <?php
    include 'footer.php';
    ?>