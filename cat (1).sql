-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 20, 2018 at 05:47 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jobcapsindia`
--

-- --------------------------------------------------------

--
-- Table structure for table `cat`
--

CREATE TABLE IF NOT EXISTS `cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `cat`
--

INSERT INTO `cat` (`id`, `cat_name`) VALUES
(1, 'IT jobs'),
(2, 'Tech jobs'),
(3, 'BPO jobs'),
(4, 'HR/Admin'),
(5, 'Digital Marketing'),
(6, 'HR Consultant'),
(7, 'Sales/Marketing'),
(8, 'Administration'),
(9, 'Business Development'),
(10, 'Software Testing'),
(11, 'Java Developer'),
(12, 'Hotels, Restaurants'),
(14, 'IT Software - Network Administration , Security'),
(15, 'Driver'),
(16, 'HR Consultant'),
(17, 'Medical/Health care'),
(18, 'Construction/supervising'),
(19, 'Other jobs'),
(21, 'Executive Assistant , Front Office'),
(22, 'Data Entry'),
(23, 'Office Assistant'),
(24, 'Cook'),
(26, 'Operator'),
(27, 'Software developers'),
(28, 'Designers'),
(29, 'Tele caller'),
(30, 'Tour & Travels'),
(31, 'Accountant'),
(32, 'Spa Therapist'),
(33, 'Data Analyst '),
(34, 'House Keeper'),
(35, 'Computer Operator '),
(36, 'Customer Care Manager '),
(37, 'Customer Relation Officer '),
(38, ''),
(39, 'Billing staff'),
(40, 'Public relations officer '),
(41, 'Academic counsellor ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
