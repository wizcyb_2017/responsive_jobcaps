<?php
session_start();
include_once 'gpConfig.php';
require_once 'fbConfig.php';


unset($_SESSION['facebook_access_token']);


unset($_SESSION['userData']);
unset($_SESSION['token']);
unset($_SESSION['userData']);

//Reset OAuth access token
$gClient->revokeToken();

session_destroy();
header('location: seekerlogin.php');

?>